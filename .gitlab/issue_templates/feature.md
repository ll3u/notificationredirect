## summary

(summarize the feature request)

## detailed description

(put all relevant details in here and describe the feature and why you want it)

## possible solution

(is there anything that is good to know about it)

## relevant logs and/or screenshots

(paste any relevant logs - please use code blocks (```) to format console output, logs, and code, as
it's very hard to read otherwise.)

/label ~suggestion
