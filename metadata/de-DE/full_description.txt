Dies ist ein Fork der App <a href="https://f-droid.org/de/packages/click.dummer.notify_to_jabber/">click.dummer.notify_to_jabber</a> a.k.a. <i>Notify2Jabber</i>, die anscheinend nicht mehr von ihrem Autor gepflegt wird.

Manchmal ist ein neues, riesiges Smartphone zu groß für Partys. Da ist es schön, wenn man alle Nachrichten auf ein kleineres, altes Gerät umleiten kann. Im Notfall kann man wenigstens per Textnachricht antworten. Diese App leitet fast alles an ein Jabber-konto, an einen Gotify-server oder per textnachricht weiter. Es ist ein kleiner Life Hack, der der FOSS-Gemeinschaft nicht vorenthalten werden sollte.

Diese App sendet alle Android-Textbenachrichtigungen an einen xmpp-Jabber-Account via <a href="https://github.com/igniterealtime/Smack/">smack</a> (Apache 2.0 Lizenz) Bibliothek.

Alternativ kann man auch einen Gotify-Server mit einem Application-Token verwenden, um die Benachrichtigungen umzuleiten. Die App verwendet die <a href="https://square.github.io/retrofit/">retrofit</a>-Bibliothek (Apache 2.0 license) für REST-Anfragen.

Andere mögliche Anwendungsfälle sind

• ein ungoogled Telefon empfängt Benachrichtigungen von kostenpflichtigen Anwendungen auf einem anderen Gerät
• ein geschäftliches Telefon leitet Benachrichtigungen an ein privates Telefon weiter