# NotificationRedirect

![Logo](app/src/main/res/mipmap-xhdpi/ic_launcher.png)

this is a fork of the app `click.dummer.notify_to_jabber` a.k.a. 
[`Notify2Jabber`](https://github.com/no-go/NotifyRelay) which seems to be no 
longer being maintained by its author.

the original intention of the application author was

```
Sometimes a new giant smartphone is too big for parties. So it is nice to be
able to redirect all messages to a smaller old device. In an emergency, you can
at least reply by text message. This app forwards almost everything to a
Jabber account, to a Gotify server or by text message. It's a little life hack
that should not be withheld from the FOSS community.
```

other possible use cases are 

- an ungoogled phone receives notifications from paid applications on another device
- business phone forwards notifications to a private phone


this app sends all android text notifications to an xmpp jabber account via
[smack](https://github.com/igniterealtime/Smack/) library.

alternatively you can use a [Gotify](https://gotify.net/) server with an 
application token to redirect the notifications. it uses the 
[retrofit](https://square.github.io/retrofit/) library for REST requests.

## Gotify with self signed certificate

when an X509Certificate is detected, the domain-name is checked and the fingerprint
is saved. further calls are compared against this fingerprint.

## Get the App

you can get a signed APK (4.4.4+) from here

[![Latest Release](https://gitlab.com/ll3u/notificationredirect/-/badges/release.svg)](https://gitlab.com/ll3u/notificationredirect/-/releases)

<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png" alt="get it on f-droid" height="80" />

> see #13


## Applications License

```
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
```

## Smack Library License

```
Use of the Smack source code is governed by the Apache License Version 2.0:

Copyright 2002-2008 Jive Software.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```

## Retrofit Library License

```
Copyright 2013 Square, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```